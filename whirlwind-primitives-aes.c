/**
 * whirlwind-primitives-as.c -- Whirlwind is a secure random number generator.  This file
 * contains wrapper routines for simple AES encryption in ECB mode.
 *
 * Copyright (C) Adam C Everspaugh <ace@cs.wisc.edu>, 2014.  All rights reserved.
 */
#include <linux/types.h>
#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include <linux/err.h>
#include "whirlwind-rng.h"
#include "whirlwind-rng-internal.h"

#define CIPHER_NAME "ecb(aes)"
#define OK 0

static struct blkcipher_desc cipher;

/**
 * Initializes an AES cipher and keys it using the provided 128-bit key.
 */
int aes_init(u8 key[AES_KEY_BYTES])
{
  unsigned int err;
    
  // Allocate a new cipher.
  cipher.tfm = crypto_alloc_blkcipher(CIPHER_NAME, 0, 0);
    
  // Check for errors.
  if (IS_ERR(cipher.tfm)) 
  {
    printk("Whirlwind RNG Primitive: failed to load transform for %s: %ld\n", CIPHER_NAME,
	   PTR_ERR(cipher.tfm));
    cipher.tfm = 0;
    return -EFAULT;
  }

  cipher.flags = 0;    

  // Key the cipher.
  err = crypto_blkcipher_setkey(cipher.tfm, key, sizeof(key));

  // Check for errors.
  if (err) {
    printk("Whirlwind RNG Primitive: setkey() failed flags=%x\n",
	   crypto_blkcipher_get_flags(cipher.tfm));
    return err;
  }
  return OK;
}
    
/**
 * Transforms an input value into an output value with AES encrypt using the key that 
 * was previously established.  The buffer contains the input value and is overwritten with
 * the output value.
 */
int aes_encrypt(u8 buffer[AES_BLOCKSIZE_BYTES])
{  
  int err;
  struct scatterlist sg;

  // Ensure that we have a blockcipher that has been properly initialized.
  if (cipher.tfm == 0)
  {
    printk("Whirlwind RNG Primitive: Cannot encrypt - blockcipher has not been initialized.");
    return -1;
  }

  // Initialize the scatterlist for for a single block.
  sg_init_table(&sg, 1);
  sg_set_buf(&sg, buffer, AES_BLOCKSIZE_BYTES);

  // Perform the encryption and check for errors.
  err = crypto_blkcipher_encrypt(&cipher, &sg, &sg, AES_BLOCKSIZE_BYTES);

  if (err) {
    printk("Whirlwing RNG Primitive: Encrypt operation failed.");
    return err;
  }

  return OK;
}

