/*
 * whirlwind-rng-output.h -- Whirlwind is a secure random number generator.  This file
 * contains routines for securely generating random output values from internal seed values.
 *
 * Copyright (C) Adam C Everspaugh <ace@cs.wisc.edu>, 2014.  All rights reserved.
 */
#include <linux/random.h>
#include <linux/sched.h>
#include <linux/cryptohash.h>
#include <linux/spinlock.h>
#include <linux/percpu.h>
#include "whirlwind-rng.h"
#include "whirlwind-rng-internal.h"

/**
 * Reserves a sequence of unique counter values for use generating
 * RNG output values using counter-mode hashing.
 */
static inline __u64
get_rng_output_counter(unsigned int length)
{
  profile_enter();
  static atomic64_t rng_output_counter = ATOMIC64_INIT(0);

  // Reserve a sequence of counter values of the specified length.
  __u64 end = atomic_long_add_return(length, &rng_output_counter);

  // Return the starting point of the sequence.
  profile_exit();
  return end - length;
}

/**
 * Generates an arbitrary length RNG output using the current seed values
 * of the RNG and computing input blocks by running the seeds through a
 * full hash function in counter-mode.
 */
inline int
rng_create_output(u8* kernel_buffer, u8* __user user_buffer, unsigned int length)
{
  profile_enter();
  static const u8 feedbackInput[INPUT_BYTES] = { 1, 0 };
  int rv = 0;
  unsigned long flags, hw_rand = 0;

  // Initial our counter mode input to the hash.
  struct counter_mode_input input =
  {
    // Reserve a sequence of counter values, and initialize our counter value
    // to the first in the sequence.
    .counter = get_rng_output_counter(length/HASH_BLOCKSIZE + 1),
    .value1  = get_cycle_counter(),
    .value2  = current->pid
  };

  // Quickly add entropy to the RNG if it hasn't been initialized.
  if (atomic_read(&rng_initialized) == 0)
    initialize_rng();

  // If there's a HW RNG, add that as well.
  if (arch_get_random_long(&hw_rand) && hw_rand)
    input.value3 = hw_rand;

  // Grab the lock on the fast seed and hash it once as a mechanism to
  // prevent backtracking attacks.  Then copy it unto the local stack.
  spin_lock_irqsave(&seed_fast.lock, flags);
  	hash_input(seed_fast.seed, feedbackInput);
  	memcpy(input.seed1, seed_fast.seed, SEED_BYTES);
	spin_unlock_irqrestore(&seed_fast.lock, flags);

  // Atomically grab the slow seed and copy it unto the stack as well.
  spin_lock_irqsave(&seed_slow.lock, flags);
  	memcpy(input.seed2, seed_slow.seed, SEED_BYTES);
	spin_unlock_irqrestore(&seed_slow.lock, flags);

  // After the seed has been copied, we add two inputs to the RNG (that won't
  // be included in this output).  
  // These inputs provide some resistance to tracking attacks by increasing
  // the uncertainty for an adversary that is rapidly sampling from the RNG.
  rng_input();

  // Generate an output value.
  rv = hash_create_output(kernel_buffer, user_buffer, length, &input);

  // Add our second input to the RNG.
  rng_input();
  profile_exit();
  return rv;
}


/**************************************************************************
 *
 * Whirlwind-AES output generation
 * 
 * This output routine produces lower-security values than 
 * rng_create_output  (128-bits of security under ideal conditions) but 
 * should be must faster under most conditions because it uses AES instead 
 * of SHA-2.  This output  routine was developed specifically to support the 
 * get_random_int interface of the Linux RNG, which needs fast 32-bit and 
 * 64-bit secure random values.
 *
 **************************************************************************/

#define AES_BLOCKSIZE_WORDS (AES_BLOCKSIZE_BYTES/4)

// Stores the key used for Whirlwind-AES so we can detect when we need to rekey the cipher.
DEFINE_PER_CPU(u32[AES_BLOCKSIZE_WORDS], aes_key);

// Stores teh previous ciphertext value so it can be chained into the next output value.
// Note: Under this implementation, the last GRI output is hanging around in memory if there's a snapshot taken.
// That means we give up on forward secrecy of the very last output value before a snapshot if the snapshot
// is compromised.
DEFINE_PER_CPU(u32[AES_BLOCKSIZE_WORDS], aes_ciphertext_chain);

/*
 * Get a random 32-bit value.  This uses a different output generation mechanism
 * than get_random_bytes.  It has less security (128-bits under ideal conditions)
 * but should typically be much faster because it uses AES instead of SHA-2.
 */
inline void
rng_create_output_aes(u32 output[AES_BLOCKSIZE_WORDS])
{
  u32 s_slow[AES_BLOCKSIZE_WORDS];
  u32* key;
  u32* chaining_value; 
  unsigned int i = 0;
  unsigned long flags;

  // Just to be confusing, we'll fill the output buffer with input values, 
  // then let the aes encryption overwrite it with the random output value
  // that we're returning to the caller.
  u32* input = output;

  // Copy the top 8 bytes of the slow seeds onto the stack.
  spin_lock_irqsave(&seed_fast.lock, flags);
    memcpy(input, seed_fast.seed, 8);
  spin_unlock_irqrestore(&seed_fast.lock, flags);

  // Atomically grab the slow seed and copy it unto the stack as well.
  spin_lock_irqsave(&seed_slow.lock, flags);
    memcpy(s_slow, seed_slow.seed, AES_BLOCKSIZE_BYTES);
  spin_unlock_irqrestore(&seed_slow.lock, flags);

  // If the slow seed has changed, we need to rekey AES.
  key = get_cpu_var(aes_key);
    for (i=0; i < AES_BLOCKSIZE_WORDS; ++i)
    {
      if (s_slow[i] != aes_key[i])
      {
        // (Re)key our cipher and store the key for future comparisons.
        aes_init((u8*)s_slow);
        memcpy(key, s_slow, AES_BLOCKSIZE_BYTES);
      }    
    }
  put_cpu_var(aes_key);

  // Construct the WW-AES input:
  // seed_fast[64] ||  CPUID[8] || CTR[24] || CC[32]
  // Note that the first 64 bytes of the fast seed are already in place.
  input[2] = smp_processor_id() << 24;
  input[2] &= get_rng_output_counter(1) & 0x00FFFFFF;
  input[3] = get_cycle_counter();

  // XOR this with the previous ciphertext chaining value.
  chaining_value = get_cpu_var(aes_ciphertext_chain);
  for (i=0; i < AES_BLOCKSIZE_WORDS; ++i)
    input[i] ^= chaining_value[i];

  // Transform our input using AES.  This overwrites the result to the 
  // output buffer provided by our caller.
  aes_encrypt((u8*)input);

  // Put our per-cpu variables back.
  put_cpu_var(chaining_value);
}

