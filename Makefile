
CC            = gcc 
CFLAGS        = -std=c99 -Wall -D__LINUX__ -ggdb
BIN           := bin

KERNEL_SOURCE := /u/a/c/ace/Scratch/linux-3.2.35-dooku64
THREADS       := 32
PWD           := $(shell pwd)

# if KERNELRELEASE is defined, we've been invoked from the
# kernel build system, so instruct the kernel to build our module.
ifneq (${KERNELRELEASE},)
        obj-m := detect-reset-kmod.o 
# Otherwise we were called directly from the command line.
# Permit the normal build system.
else

# What to build
build: fast

# Just build the tools that compile quickly.
fast:  rnd-ec rnd-reset rnd-time detect-reset

# Disable builds
none: 

# Builds everything
all: rnd-reset rnd-ec kernel kernel-module

# Build our C-based command line tools.
detect-reset: rnd-tools.h detect-reset.h detect-reset.c
	@${CC} ${CFLAGS} detect-reset.c -o ${BIN}/$@

rnd-time: rnd-tools.h rnd-time.c
	@${CC} ${CFLAGS} rnd-time.c -o ${BIN}/$@

rnd-reset: random.h rnd-tools.h rnd-reset.c
	@${CC} ${CFLAGS} rnd-reset.c -o ${BIN}/$@

rnd-ec: random.h rnd-ioctl.h rnd-tools.h rnd-ec.c
	@${CC} ${CFLAGS} rnd-ec.c -o ${BIN}/$@

# Build the entire instrumented kernel. 
kernel:
	@${MAKE} -C ${KERNEL_SOURCE} -j ${THREADS}

# Build our loadeable kernel module.
kernel-module:
	@${MAKE} -C ${KERNEL_SOURCE} SUBDIRS=${PWD} modules

.phony: all build fast kernel kernel-module none 
endif
