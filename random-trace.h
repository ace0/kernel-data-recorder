/**
 * random-trace.h
 * Adam C Everspaugh
 * Copyright (C) 2013 - All rights reserved.
 *
 * Contains instrumentation macros and settings.
 */
#ifndef RANDOM_TRACE_H_
#define RANDOM_TRACE_H_

// Do something exactly once during program execution.
#define ONCE(action) ({     \
	static int __execute = 1; \
	if (__execute)            \
	{                         \
		action;                 \
		__execute = 0;          \
	}                         \
})

// Log the state of one of the entropy pools.
#define dtrace_pool(debug, store, event) ({                        \
	dtraceb(debug, store.pool, store.poolinfo->POOLBYTES,            \
		"event:%s, pool:%s, input_rotate:%u, add_ptr:%u, "             \
		"initialized:%d, entropy_total:%d, entropy_count:%d, "         \
		"poolbytes:%s",                                                \
		event, store.name, store.input_rotate, store.add_ptr,          \
		store.initialized, store.entropy_total, store.entropy_count ); \
	dtraceb(debug, store.last_data, EXTRACT_SIZE,                    \
		"event:%s, pool:%s, last_data:%s",                             \
		event, store.name );                                           \
})

// Log the state of the fast mix pool.
#define dtrace_fpool(debug, fp, event) ({                          \
	dtraceb(debug, fp->pool, 16,                                     \
	  "event:%s, pool:fast_pool, count:%d, rotate:%d, last:%ld, "    \
    "last_timer_intr:%d, poolbytes:%s",                            \
		event, fp->count, fp->rotate, fp->last, fp->last_timer_intr);  \
})

// Converts a function into a NOP if the enabled flag is set to false.
#define NOP(enabled) ({  \
	if (!enabled) return;  \
})

// Similar to NOP, but uses a specified return value.
#define NOP_RV(enabled, rv) ({ \
	if (!enabled) return rv;     \
})

// Configure individual RNG inputs and other functions
#define ADD_DEVICE_RAND    true
#define ADD_INTERRUPT_RAND true
#define ADD_TIMER_RAND     true
#define ADD_INPUT_RAND     true
#define ADD_DISK_RAND      true
#define EXTRACT_BUFFER     true
#define WRITE_POOL         true

// Configure additional trace data
#define ADD_DEVICED			    false
#define ADD_TIMERD			    false
#define ADD_INTERRUPTD 	    false
#define EXTRACT_BUFD        false
#define EXTRACT_ENTROPY     false
#define FAST_MIXD           false
#define FAST_MIX_INTERNAL_D false
#define MIX_POOLD           false
#define PRE_GRBD            false
#define POST_GRBD           false
#define URANDOM_READ        false
#define XFER_POOL           false

// Force fast_mix to use a fixed, pre-defined input.
//#define FAST_MIX_FIXED_INPUT
#undef FAST_MIX_FIXED_INPUT

#endif // RANDOM_TRACE_H_
