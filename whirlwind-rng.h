/*
 * whirlwind-rng.h -- A secure random number generator
 *
 * Copyright (C) Adam C Everspaugh <ace@cs.wisc.edu>, 2014.  All rights reserved.
 */
#ifndef _WW_RNG
#define _WW_RNG

#include <crypto/hash.h>

#define HASH_BLOCKSIZE      SHA512_DIGEST_SIZE
#define AES_BLOCKSIZE_BYTES 16
#define AES_BLOCKSIZE_WORDS (AES_BLOCKSIZE_BYTES/4)

// Runtime profiling logged with printk when PROFILE is defined.
//#define PROFILE
#undef PROFILE

#ifdef PROFILE
#include <linux/kernel.h>

# define profile_enter()			\
  cycles_t __enter_cycles = get_cycles()

# define profile_exit()				\
  printk(KERN_DEBUG "RNG PROFILE %s:%llu\n", __FUNCTION__, (get_cycles()-__enter_cycles))
#else
# define profile_enter() static int __unused = 0
# define profile_exit()  __unused += 1
#endif

inline int rng_create_output(u8* kernel_buffer, u8* __user user_buffer, unsigned int length);
inline void rng_create_output_aes(u32 output[AES_BLOCKSIZE_WORDS]);

#endif // _WW_RNG
