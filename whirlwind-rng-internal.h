/*
 * whirlwind-rng-internal.h -- Whirlwind is a secure random number generator.  This file
 * contains routines common datatypes for internal use.  For the "public" interface to 
 * to the Whirlwind RNG, see: whirwind-rng.h.
 *
 * Copyright (C) Adam C Everspaugh <ace@cs.wisc.edu>, 2014.  All rights reserved.
 */
#ifndef _WW_RNG_INTERNAL
#define _WW_RNG_INTERNAL

#include <linux/types.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <crypto/hash.h>
#include <crypto/sha.h>

// Use SHA512 as our cryptographic hash primitive.
#define SEED_BYTES      SHA512_DIGEST_SIZE
#define INPUT_BYTES     SHA512_BLOCK_SIZE

// AES is used to generate low-security (and shorter) outputs.
#define AES_KEY_BYTES 16

// Zero-out sensitive memory.
#define zmem(var) memset(var, 0, sizeof(var))

/**
 * Retrieves the current cycle counter, if available, otherwise grabs the kernel 
 * timer (jiffies).
 */
static inline u64
get_cycle_counter(void)
{
  cycles_t cycles = get_cycles();
  return (cycles) ? cycles : jiffies;
}

/**
 * Input message used to create output values in PRNG-counter-mode.
 */
struct counter_mode_input
{
  u8 seed1[SEED_BYTES];  // Dueling seed values.
  u8 seed2[SEED_BYTES];  
  u64 counter;           // Starting counter value for this particular seed.
  u64 value1;            // + 192-bits of caller-provided data.
  u64 value2;
  u64 value3;
};

/**
 * An input value that can be added to the system's random number generator.
 */
struct rng_input
{
  // Unique value that identifies the source of each input.
  u32 source_id;

  // Lower 4 bytes if of tycle counter at the time the input sample is contributed.
  u32 cycles;

  // 64-bits of source-contributed data.  These can be any values that are expected
  // to be hard to predict.
  u32 value1;
  u32 value2;
};

struct seed_pool
{
  // Current (public) seed value for output generation.
  u8 seed[SEED_BYTES];

  // An internal seed value hidden from output generation.  Only used if
  // min_hashes > 1.
  u8 __seed_internal[SEED_BYTES];

  // The number of times the seed pool has been hashed.
  unsigned int hash_count;

  // The minimum number of times the internal seed must be hashed before making
  // the seed "public".
  const unsigned int min_hashes;

  // Write lock for this seed.
  spinlock_t lock;
};

// Shared values
extern struct seed_pool seed_fast;
extern struct seed_pool seed_slow;
extern atomic_t rng_initialized;

inline void initialize_rng(void);

// Hash primitives
inline void hash_input(u8 seed[SEED_BYTES], const u8 input[INPUT_BYTES]);
inline int hash_create_output(u8* kernel_buffer, u8* __user user_buffer, const unsigned int size,
			       struct counter_mode_input* input);

// AES primitives
int aes_init(u8 key[AES_KEY_BYTES]);
int aes_encrypt(u8 buffer[AES_BLOCKSIZE_BYTES]);

#endif // _WW_RNG_INTERNAL
