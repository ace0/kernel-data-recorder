/*
 * whirlwind-rng-input.h -- Whirlwind is a secure random number generator.  This file
 * contains routines for securely extracting randomness from biased inputs.
 *
 * Copyright (C) Adam C Everspaugh <ace@cs.wisc.edu>, 2014.  All rights reserved.
 */
#include <linux/random.h>
#include <linux/sched.h>
#include <linux/cryptohash.h>
#include <linux/spinlock.h>
#include <linux/percpu.h>
#include "whirlwind-rng.h"
#include "whirlwind-rng-internal.h"

// Buffer and index for accumulating unhashed inputs to the fast pool.
DEFINE_PER_CPU(u8[INPUT_BYTES], input_fast);
DEFINE_PER_CPU(unsigned int, write_index_fast);

// Buffer and index for accumulating unhashed inputs to the slow pool.
DEFINE_PER_CPU(u8[INPUT_BYTES], input_slow);
DEFINE_PER_CPU(unsigned int, write_index_slow);

// The fast seed is really fast- all hashes are immediately exposed as public.
struct seed_pool seed_fast = 
{
  .hash_count  = 0,
  .min_hashes  = 1,
  .lock        = __SPIN_LOCK_UNLOCKED(&seed_fast.lock)
};

// Hold back 10x as many inputs for the slow seed to increase an attacker's
// complexity of predicting all inputs.
struct seed_pool seed_slow = 
{
  .hash_count  = 0,
  .min_hashes  = 10,
  .lock        = __SPIN_LOCK_UNLOCKED(&seed_slow.lock),
};

// Indicates whether the RNG has been initialized after boot.
atomic_t rng_initialized = ATOMIC_INIT(0);

/**
 * Quickly add entropy to an RNG.
 */
inline void
initialize_rng(void)
{
	// TODO: Add QuickRNG
}

/** 
 * Hashes the contents of a seed pool.  If the hash count matches the minimum
 * hash count, then the internal seed value is made "public" - available
 * for output generation.
 */
static inline void hash_input_buffer(const u8 input[INPUT_BYTES], struct seed_pool* pool)
{
  unsigned long flags;

  // Grab the lock for each hash.
  spin_lock_irqsave(&pool->lock, flags);

  // Unless min-hashes is > 1, just hash the input directly into the 
  // seed and quit.
  if (pool->min_hashes <= 1)
  {
    // Hash the input buffer into the seed.
    hash_input(pool->seed, input);
    spin_unlock_irqrestore(&pool->lock, flags);
    return;
  }

  // Otherwise, hash into the internal seed and increment the hash counter.
  hash_input(pool->__seed_internal, input);
  pool->hash_count += 1;
  
  // Make the internal seed "public" whenever the minimum number of hashes
  // has occurred.
  if (pool->hash_count % pool->min_hashes == 0)
  {
    memcpy(pool->seed, pool->__seed_internal, SEED_BYTES);
  }

  spin_unlock_irqrestore(&pool->lock, flags);
}

/**
 * Adds the contents of rng_input to a given seed pool.
 * If the input buffer is full, the contents will hashed into a new seed
 * seed value.  If the hash count matches the min-hash count, the seed value is
 * made "public".
 */
static inline void
add_input_to_pool(const struct rng_input* input, struct seed_pool* pool)
{
  profile_enter();
  unsigned int length=0, index = 0, space_available;
  u8* input_pool = NULL;

  // The length of the input is not to exceed the size of the pool.
  const unsigned int input_length = min_t(unsigned int, sizeof(struct rng_input),
  		INPUT_BYTES);

  // Basic quality control on the input parameter- discard null pointers
  // or inputs that are identically 0.
  if (input == 0 || 
      input->source_id + input->cycles + input->value1 + input->value2 == 0)
  {
    return;
  }

  // Grab our input buffer and the current write_index.
  if (pool == &seed_fast)
  {
    index = get_cpu_var(write_index_fast) % INPUT_BYTES;
    input_pool = get_cpu_var(input_fast);
  }
  else
  {
    index = get_cpu_var(write_index_slow) % INPUT_BYTES;
    input_pool = get_cpu_var(input_slow);
  }

  // Determine the space available in the pool and the maximum length
  // we can write.
  space_available = INPUT_BYTES - index;
  length = min_t(unsigned int, space_available, input_length);

  // Write to the pool.
  memcpy(input_pool + index, input, length);

  // If the input pool is full, then hash the contents now.
  if (length >= space_available)
  {
    hash_input_buffer(input_pool, pool);
  }

  // If we had to truncate our write, wrap around and overwrite
  // values from the start of the pool.
  if (length > space_available)
  {
    memcpy(input_pool, input + length, input_length - length);
  }
 
  // Put our per-cpu variables away.
  if (pool == &seed_fast)
  {
  	get_cpu_var(write_index_fast) += length;
    put_cpu_var(write_index_fast);
  	put_cpu_var(input_fast);
  }
  else
  {
  	get_cpu_var(write_index_slow) += length;
    put_cpu_var(write_index_slow);
  	put_cpu_var(input_slow);
  }

  profile_exit();
}

/**
 * Selects the proper input seed pool and adds an input to that pool.
 */
static inline void
rng_add_input(const struct rng_input* input)
{
  // How often inputs are diverted from the fast input buffer to the slow buffer.
  // This must be >= 2.  The larger this value, the longer it takes before the slow 
  // seed  is integrated into output generation.  The smaller this value, the more 
  // inputs are "stolen" from the fast seed and so this impacts how quickly the fast 
  // seed changes.
  const static unsigned int slow_seed_inputs = 10;

  // The total number of inputs we've added.
  static atomic_t input_count = ATOMIC_INIT(0);

  // Increment the counter, select a pool, and add the input.
  unsigned int count = atomic_inc_return(&input_count);
  struct seed_pool* pool = (count % slow_seed_inputs == 0) ? &seed_slow : &seed_fast;
  add_input_to_pool(input, pool);
}

/**
 * Add a new input to the random number generator.
 * @source_id: A unique identifier for this source- ideally, 
 * use __COUNTER__.  Better yet, just use rng_add*() macros.
 * @value1: 32 bits of source provided data.
 * @value2: 32 more bits of source provided data.
 */
void __rng_input_64(u32 source_id, u32 value1, u32 value2)
{
  // Add the register instruction pointer, the current cycle counter,
  // and the caller-provided values into the RNG.
  struct rng_input input =
    {
      .source_id = source_id,
      .cycles    = get_cycle_counter(),
      .value1    = value1,
      .value2    = value2
    };
  rng_add_input(&input);
}
