/**
 * detect-reset.h
 *
 * Copyright (C) 2013 - Adam C Everspaugh
 * All rights reserved.
 */
#include "detect-reset.h"
#include "rnd-ioctl.h"

#define BUFFER_SIZE 5*MB
static u8 samples[BUFFER_SIZE];
static unsigned long sample_times[BUFFER_SIZE];


// Use the cycle counter to detect a VM reset in a userland process.
int main(int argc, char** argv)
{
  // Check for arguments.
  if (argc != 5)
  {
    printf(
   "Usage: detect-reset <sample size> <sample interval> <samples> <RNG>\n\n"
   "sample size:      Size of sample (bytes) to take from the RNG\n"
   "sample interval:  Interval (microseconds) between taking from the RNG\n"
   "samples:          The number of samples to take from the RNG\n"
   "RNG:              Selects the RNG to sample: 1=/dev/urandom ; 2=/dev/random\n"
    );
    return ERROR;
  }
  
  // Parse our arguments.
  int sample_size         = atoi(argv[1]);
  int sample_interval_us  = atoi(argv[2]);
  int sample_count        = atoi(argv[3]);
  int rng                 = atoi(argv[4]);
  char* RNG = (rng == 1) ? DEV_URANDOM : DEV_RANDOM;
  
  // Verify that we have enough space for these samples.
  confirm(sample_size*sample_count <= BUFFER_SIZE,
	  "Combined sample size (size*count) is %.2fMB, but cannot exceed %.2fMB",
	  (sample_size*sample_count)*1.0/MB, BUFFER_SIZE*1.0 / MB);
  
  // Open up the RNG for reading.
  int fd = open_device(RNG);
  
  // Grab the current time.
  time_t now;
  time(&now);

  // Print status before reset.
  printf("System clock before reset:  %s", ctime(&now));
  printf("Cycle counter before reset: %lu\n\n", rdtsc());

  //
  // Wait for a reset.
  //
  wait_for_reset();
  
  //
  // Wake up!
  //
  
  // Grab the system time, cycle counter, and entropy count at wake-up.
  time(&now);
  u64 cycles = rdtsc();

  int entropy_count = -1;
  #ifdef __LINUX__
   entropy_count = get_entropy_count(fd, RNDGETENTCNT);
  #endif

  // Pause before each sample, and then read from the RNG.
  for (int i=0; i < sample_count; ++i)
  {
    // Wait for the specified interval, then take a sample from the RNG.
    busy_wait(sample_interval_us);
    read_device(fd, &samples[i*sample_size], sample_size);
    sample_times[i] = get_time_us();
  }

  //
  // Print our results.
  //

  // Start with the wakeup time.
  printf("System clock at wakeup:     %s\n",  ctime(&now));
  printf("Cycle counter at wakeup:    %lu\n", cycles);
  printf("Time of day (us) at wakeup: %lu\n", get_time_us());
  printf("Input pool entropy count:   %d\n",  entropy_count);
  
  // Print the RNG samples.
  for(int i=0; i < sample_count; ++i)
  {
    printf("%s %02d, bytes=%d, ", RNG, i, sample_size);
    print_hex(&samples[i*sample_size], sample_size);
    printf(" time=%lu us\n", sample_times[i]);
  }
  
  // Close the RNG and report success.
  close(fd);
  return OK;
}

