/**
 * data_recorder.c
 * Copyright (C) 2013 Adam C Everspaugh. All rights reserved.
 * February 6, 2013 - Initial creation.
 */
#include "data_recorder.h"
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/kdev_t.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/jiffies.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/version.h>

// Teardown flags
#define REGION 1
#define CLASS  2
#define DEVICE 4

// Constants and debug flag
#define DEBUG 0                     // Turns on/off module debugging
#define DEVICE_COUNT 1              // Create 1 device for retrieving log data
#define DEVICE_NAME "data-recorder" // Name of our device file in /dev
#define NEWLINE "\n"                // Guess what this is
#define MB  (1024*1024)             // The number of bytes in a megabyte
#define MESSAGE_TABLE_SIZE (100*MB) // The amount of memory reserved for log messages

#if DEBUG // Enable module debugging statements
# define PDEBUG(fmt, args...) printk( KERN_INFO "Data recorder: " fmt "\n", ## args)
#else // Disable module debugging statements
# define PDEBUG(fmt, args...)
#endif

static dev_t device_no;             // Device number
static struct cdev char_device;     // Data recorder character device
static struct class * device_class; // Data recorder device class
static int recording = true;        // Turns logging on and off

static char message_table[MESSAGE_TABLE_SIZE];  // Store log messages in a single table
static char* write_ptr = message_table;         // Pointer for writing to the talbe
static char* read_ptr = message_table;          // Pointer for reading from the table

// Local lock for write_ptr and the text scratch buffer.
static DEFINE_SPINLOCK(lock);

/**
 * Create the hexadecimal string from a block of memory.
 */
void hex_from_buffer(char* text, int text_size, const void* buffer, int size)
{
	int i = 0, rv = 0;

	// Formatting an empty buffer is easy!
	if (buffer == NULL || size <= 0) { return; }

	// Ensure our other parameters are sane.
	if (text == NULL || text_size <= 0 ) { return; }

	// The total number of bytes the be generated is the minimum of:
	// -Number of bytes in the buffer
	// -Half the output buffer (each byte requires 2-hex characters)
	//  minus a single byte for a string terminator (T2000)
	size = min(text_size/2 -1, size);

	// Print the buffer one byte at a time.
	for (i=0; i < size; ++i)
	{
		rv = snprintf(text+2*i, 3, "%02X", ((unsigned char*)buffer)[i]);
		if (rv < 0)
		{
			PDEBUG("Error formatting buffer");
		}
	}
}
EXPORT_SYMBOL(hex_from_buffer);

/**
 * Writes an entry to the in-memory log.
 */
int log_msg(const char* message, ...)
{
	int message_length = 0, rv = 0;
	unsigned int space_available = 0, max = 0;
	unsigned long flags;
	va_list args;

	// Don't bother logging if recording has been disabled.
	if (!recording)	{ return -ENOTLOGGED; }

	// Grab a lock while we work with the message table and write pointer.
	spin_lock_irqsave(&lock, flags);
	{
		// Calculate the space available in message table.
		space_available = message_table + MESSAGE_TABLE_SIZE - write_ptr;

		// The maximum number of bytes we'll let vsnprintf output.
		max = min_t(uint, MAX_MESSAGE_SIZE, space_available);

		va_start(args, message); // Initialize our variable argument list

		// Format the message and insert it into the log table.
		message_length = vsnprintf(write_ptr, max, message, args);

		va_end(args); // Done with argument list

		// Check for vsnprintf errors
		if (message_length < 0)
		{
			PDEBUG("Error formatting message - message not logged");
			rv = -ENOTLOGGED;
			goto out;
		}

		// vsnprintf reports the maximum desired string length, not the maximum length,
		// in the case of clipping.  Recalculate the actual message length.
		message_length = min_t(uint, message_length + 1, max);

		// See if the message fits into the table.
		if (message_length > space_available)
		{
			// If not- record an error and disable logging.
			PDEBUG("Message exceeds available log size - message not logged and recording disabled");
			recording = false;
			rv = -ENOTLOGGED;
			goto out;
		}

		// See if the messages was clipped because it exceed the max message size.
		if (message_length > MAX_MESSAGE_SIZE)
		{
			PDEBUG("Message exceeds maximum message size - message not logged.");
			rv = -ENOTLOGGED;
			goto out;
		}

		// Everything went well- advance the write pointer to commit the message to the table.
		write_ptr += message_length;
	}

// Release the lock and quit.
out:
	spin_unlock_irqrestore(&lock, flags);
	return rv;
}
EXPORT_SYMBOL(log_msg);

/**
 * Clears the logs and enables log recording.
 */
void log_reset()
{
	ulong flags;
	PDEBUG("Clearing log and enabling recording");

	// Grab a lock and reset the write_ptr.
	spin_lock_irqsave(&lock, flags);
	{
		write_ptr = message_table;
		read_ptr = message_table;
		recording = true;
	}
	spin_unlock_irqrestore(&lock, flags);
}
EXPORT_SYMBOL(log_reset);

/**
 * Open the device file: halts all future logging.
 */
static int device_file_open(struct inode* i, struct file* f)
{
	PDEBUG("File opened; logging halted");

	// Turn off logging.
	recording = false;
	return 0;
}

/**
 * Close the device file: does nothing.
 */
static int device_file_close(struct inode* i, struct file* f)
{
	return 0;
}

/**
 * Read formatted log entries.
 */
static ssize_t device_file_read(struct file* f, char __user *buffer, size_t buffer_l, loff_t* offset)
{
	// Static pointer for reading from the message table across multiple calls to
	// device_file_read
	char* buffer_ptr = buffer;  // Our pointer for insertion into the user's buffer
	unsigned int message_length = 0;     // Length of a single log message

	PDEBUG("device_file_read with user buffer length %ld bytes", buffer_l);

	// Keep reading until there are no more messages in the table to read.
	while (read_ptr < write_ptr)
	{
		// Determine the length of the next message.  Add 1 to account for the \0 (which
		// we'll change to a \n).
		message_length = strlen(read_ptr) + 1;
		PDEBUG("Reading message of length %d", message_length);

		if (buffer_ptr + message_length > buffer + buffer_l)
		{
			PDEBUG("Next log entry will exceed user buffer - short circuiting");
			break;
		}

		// Copy the message to the user's buffer
		if (copy_to_user(buffer_ptr, read_ptr, message_length) > 0)
		{
			// Copy failed - quit
			PDEBUG("copy_to_user failed: aborting output with error EIO");
			return -EIO;
		}

		// Replace the final \0 with a \n.
		if (copy_to_user(buffer_ptr + message_length - 1, NEWLINE, 1) > 0)
		{
			// Copy failed - quit
			PDEBUG("copy_to_user failed on newline: aborting output with error EIO");
			return -EIO;
		}

		buffer_ptr += message_length;
		read_ptr   += message_length;
	}

	// Return the number of bytes written.
	PDEBUG("Finished reading %ld bytes", (buffer_ptr - buffer));
	return (buffer_ptr - buffer);
}

/**
 * Write to the device file: does nothing.
 */
static ssize_t device_file_write(struct file* f, const char __user *b, size_t l, loff_t* o)
{
	return l;
}

// Our file operations
static struct file_operations device_file_file_ops =
{
	.owner = THIS_MODULE,
	.open = device_file_open,
	.release = device_file_close,
	.read = device_file_read,
	.write = device_file_write
};

/**
 * Destroys character device region, class, and device as requested.
 * Returns -EIO every time.
 */
static int teardown(int destroy)
{
	// Destroy the device
	if (destroy & DEVICE)
	{
		device_destroy(device_class, device_no);
	}

	// Destroy the class
	if (destroy & CLASS)
	{
	  class_destroy(device_class);
	}

	// Unregister the device region
	if (destroy & REGION)
	{
		unregister_chrdev_region(device_no, DEVICE_COUNT);
	}
	return -EIO;
}

/**
 * Module constructor: registers a character device driver so that logged data can
 * be read from /dev/data-recorder
 */
static int __init device_file_init(void)
{
	PDEBUG("Initialized with %d bytes of memory", MESSAGE_TABLE_SIZE);

	// Register a single character device.
	if (alloc_chrdev_region(&device_no, 0, DEVICE_COUNT, DEVICE_NAME) < 0)
	{
	    return -EIO;
	}
	// Create the device class
	if ((device_class = class_create(THIS_MODULE, "chardrv")) == NULL)
	{
		return teardown(REGION);
	}
	// Create the device
	if (device_create(device_class, NULL, device_no, NULL, DEVICE_NAME) == NULL)
	{
		return teardown(CLASS | REGION);
	}
	// Initialize and add the character device
	cdev_init(&char_device, &device_file_file_ops);
	if (cdev_add(&char_device, device_no, DEVICE_COUNT) < 0)
	{
		return teardown(DEVICE | CLASS | REGION);
	}

	PDEBUG("Device registered <Major:%d, Minor:%d> /dev/%s", MAJOR(device_no), MINOR(device_no), DEVICE_NAME);
  return 0;
}

/**
 * Module destructor: removes/unregisters the data recorder device.
 */
static void __exit device_file_exit(void)
{
	// Delete the device, then remove the device, class, and region/
	cdev_del(&char_device);
	teardown(DEVICE | CLASS | REGION);

	PDEBUG("Device unregistered");
}

// Module definitions
module_init(device_file_init);
module_exit(device_file_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Adam C Everspaugh <ace@cs.wisc.edu>");
MODULE_DESCRIPTION("High-speed, fixed-overhead data recorder for the Linux kernel.   \
	Allows extensive logging with minimal memory footprint and buffers all log entries \
	in memory until they are retrieved.");
