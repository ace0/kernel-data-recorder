/**
 * detect-reset-kmod.c
 *
 * Kernel module to detect a VM reset and consume random bytes from the kernel's RNG.
 * 
 * Copyright (C) 2013 - Adam C Everspaugh
 * All rights reserved.
 */
#include <linux/random.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include "detect-reset.h"

// Wait time when polling for VM resets.
#ifndef WAIT
# define WAIT 500
#endif

// The number of bytes to request from get_random_bytes()
#ifndef BYTES
# define BYTES 32
#endif

static unsigned char buffer[BYTES];

/**
 * Module init: Wait for VM reset and then request random output from get_random_bytes.
 */
static int __init detect_init(void)
{
  // Wait for reset.
  printk(KERN_DEBUG "RESET-LISTENER: Waiting for VM reset\n");
  wait_for_reset(WAIT);

  // Get random bytes and log them result.
  get_random_bytes(buffer, BYTES);
  printk(KERN_DEBUG "Detected reset.  Random output: %*pHN\n", BYTES, buffer);
  return 0;
}

static void __exit detect_exit(void)
{
  // Do nothing.
}

// Module definitions
module_init(detect_init);
module_exit(detect_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Adam C Everspaugh <ace@cs.wisc.edu>");
MODULE_DESCRIPTION("Kernel module that detects VM resets and consumes and logs "
 "random numbers from the kernel RNG.");
