/**
 * rnd-tools.h
 *
 * Copyright (C) 2013 - Adam C Everspaugh
 * All rights reserved.
 * 
 * Common routines for tools that involve the system's RNG.
 */
#ifndef RND_TOOLS_H_
#define RND_TOOLS_H_

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

// Shorthand types.
typedef unsigned char u8;
typedef unsigned int u32;
typedef unsigned long long int u64;

typedef unsigned int uint;
typedef unsigned long ulong;

// Constants
#define DEV_RANDOM "/dev/random"
#define DEV_URANDOM "/dev/urandom"

#define OK 0
#define ERROR 1

#define MB  (1024*1024)
#define MSG_SIZE 1000

// Get the cycle counter.
static inline u64 rdtsc(void)
{
  u64 x = 0;
  u32 a, d;
  __asm__ volatile("rdtsc" : "=a" (a), "=d" (d));
  return ((u64)a) | (((u64)d) << 32);
}

// Check for critical error condition.  If is_error evaluates to true,
// print an error message and exit with an exit condition of 1.
#define confirm(condition, message, ...)				\
  if (!(condition))							\
    {									\
     char err_message[MSG_SIZE];					\
     sprintf(err_message, message, ##__VA_ARGS__);			\
     perror(err_message);						\
     exit(1);								\
     }

// Opens /dev/random and returns a file descriptor.  Exits
// with an error if the file could not be opened.
static inline int
open_device(char* device)
{
  // Open the file
  int fd = open(device, O_RDONLY);
  confirm(fd > 0, "Failed to open %s", device);
  return fd;
}

// Reads the specified number of bytes from the given file descriptor into a
// a buffer and checks for errors.  Exits immediately if an error is detected.
static void inline
read_device(int fd, void* buffer, const unsigned int size)
{
	// Read from the device and check for errors.
	int bytes_read = read(fd, buffer, size);
	confirm(bytes_read != -1, "Error reading from RNG");
}

// Prints the contents of an array of bytes to STDOUT in hexadecimal format.
static void inline
print_hex(const unsigned char const* buffer, const int size)
{
	for (int i = 0; i < size; ++i)
		printf("%02X", buffer[i]);
}
#endif /* RND_TOOLS_H_ */
