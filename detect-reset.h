/**
 * reset-detector.h
 *
 * Copyright (C) 2013 - Adam C Everspaugh
 * All rights reserved.
 * 
 * Use the cycle counter to detect when a VM has been reset from a snapshot
 * by detecting the large discrepancy in reported cycle counter.
 */
#ifndef DETECT_RESET_H_
#define DETECT_RESET_H_

#include "rnd-tools.h"
#include <sys/time.h>


// Threshhold for detecting VM reset
// Currently: 6600000000 ticks = 2s on a 3.3 GHz CPU when using RDTSC
#define MAX 6600000000ull


// Busy wait until a reset is detected.
static inline void wait_for_reset()
{
  u64 previous = rdtsc();
  while (1)
  {
    // If we see a huge jump in cycle counter, then we've detected a VM-reset.
    u64 current = rdtsc();    
    if ((current-previous) > MAX) 
      return;

    // Otherwise, keep checking.
    previous = current;
  }
}

// Get the time of day in microseconds.
static inline unsigned long 
get_time_us()
{
  struct timeval t;
  gettimeofday(&t, NULL);
  return t.tv_sec * 1000000 + t.tv_usec;
}

// Busy sleep for the specified number of microseconds.
static inline void busy_wait(unsigned long wait_us)
{
  unsigned long start = get_time_us();
  unsigned long current = start;
  while ((current - start) < wait_us)
  {
    current = get_time_us();
  }
}

#endif // DETECT_RESET_H_
