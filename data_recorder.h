#ifndef __DATA_RECORDER_H
#define __DATA_RECORDER_H

#include <linux/sched.h>

void hex_from_buffer(char* text, int text_size, const void* buffer, int size);
int log_msg(const char* message, ...);
void log_reset(void);

// The maximum size, in bytes, of any single log message and hex buffer.
#define HEX_BUFFER_SIZE  1400
#define MAX_MESSAGE_SIZE (HEX_BUFFER_SIZE+200)

// Error code: Message was not added to the log
#define ENOTLOGGED 512

/**
 * Logs a message with time (cycle counter), function name, and
 * the function's caller in JSON format prepended to each log message.
 */
#define trace(msg, arg...) ({                                               \
		log_msg("{ cycle_count:%llu, function:%s, caller:%pS, " msg " }",       \
				get_cycles(), __FUNCTION__,  __builtin_return_address(0), ## arg ); \
})

/**
 * Logs a buffer and a message along with the fields described above.
 */
#define traceb(buffer, size, msg, arg...) ({                   \
  unsigned long __flags;	                                     \
  spin_lock_irqsave(&hex_buffer_lock, __flags);                \
  hex_from_buffer(hex_buffer, HEX_BUFFER_SIZE, buffer, size);  \
  trace(msg, ## arg, hex_buffer);                              \
  spin_unlock_irqrestore(&hex_buffer_lock, __flags);           \
})

/**
 * Logs a buffer and a message, but sets the caller to 'none' for
 * situations where __builtin_return_address(0) fails.
 */
#define traceb_nc(buffer, size, msg, arg...) ({                        \
	unsigned long __flags;	                                             \
	spin_lock_irqsave(&hex_buffer_lock, __flags);                        \
	hex_from_buffer(hex_buffer, HEX_BUFFER_SIZE, buffer, size);          \
	log_msg("{ cycle_count:%llu, function:%s, caller:none, " msg " }",   \
			get_cycles(), __FUNCTION__, ## arg, hex_buffer);                 \
  spin_unlock_irqrestore(&hex_buffer_lock, __flags);                   \
})

/**
 * Logs a buffer if the flag is set to true.
 */
#define dtraceb(flag, buffer, size, msg, arg...) \
	if (flag) { traceb(buffer, size, msg, ## arg); }

/**
 * Logs a message if the flag is set to true.
 */
#define dtrace(flag, msg, arg...) \
	if (flag) { trace(msg, ## arg); }

#endif // __DATA_RECORDER_H
